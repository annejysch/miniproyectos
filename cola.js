/*
Define una funcion proximoEnLaFila que tome un arreglo(arreglo) y un numero(elemento)
como argumentos.Agrega el numero al final del arreglo y luego elimina el primer elemento 
del arreglo. La funcion proximoEnLaFila debe retorar el elemento removido
*/

function proximoEnLaFila(arreglo, elemento){

/*
@param push(elemento) para poder añadir un elemento al final de la cola
@return {shift()}, nos devuelve el primero elemento eliminado
*/    

//añadiendo elemento de ultimo
arreglo.push(elemento);

//eliminar al primero de la fila y retornarlo
return arreglo.shift();

}

var proximo = [1,2,3,4,5];
//mostrando en consola 
console.log("Antes"+JSON.stringify(proximo))
//cambiamos el arreglo y llenamos los parametros
console.log(proximoEnLaFila(proximo,6))
console.log("Despues"+JSON.stringify(proximo))

