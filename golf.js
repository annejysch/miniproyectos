/*
En el juego del golf cada hoyo tiene un par que representa 
el numero promedio de golpes que se espera que tenga un 
golfista para inducir la pelota al hoyo 

Hay nombre diferente dependiendo de que tan ṕor encima o debajo 
del par esten sus golpes. Tu funcion tomara par y golpes.

Retorna la cadena correcta segun esta tabla que muestra que
muestra los golpes de mayor a menor prioridad

Golpes          Retornar
--------------------------
1               "Hole-in-one!""
<= par - 2      "Eagle"
par - 1         "Birdie"
par             "Par"
par + 1         "Bogey"
par + 2         "Double bogey"
> = par + 3     "Go Home!"
*/

function golfsito(par, golpes){

    //condicion
    if(golpes == 1){
        return golpes + "\nHole-in-one" +golpes; 
    } else if(golpes <= par - 2 ){
        return golpes + "\nEagle";
    }  else if(golpes <= par - 1 ){
        return golpes + "\nBirdie";
    }  else if(golpes == par ){
        return golpes + "\nPar";
    } else if(golpes == par + 1 ){
        return golpes + "\nBogey";
    } else if(golpes == par + 2 ){
        return golpes + "\nDouble bogey";
    } else if (golpes >= par + 3 ) {
        return golpes + "\nGo home!";
    } else {
        return "No existe el numero de golpes"
    }
}


console.log(golfsito(2,5))
console.log(golfsito(6,15))
console.log(golfsito(8,0))
console.log(golfsito(4,1))

/*
Algoritmo

Inicio
    Definir par. golpe
    Si (golpe = 1)
        Retorna "Hole-in-one!" 
    Si no (golpe <= par - 2 )
        Retorna Eagle"
    Si no (golpe par - 1 )
        Retorna   "Birdie"
    Si no (par)
        Retorna "Par"
    Si no (par + 1)
        Retorna "Bogey"
    Si no (par + 2)
        Retorna "Double bogey"
    Si no (> = par + 3)
        Retorna "Go Home!"
Fin

*/